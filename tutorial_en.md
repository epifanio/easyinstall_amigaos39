## Installing AmigaOS 3.9 in an SD Card with FS-UAE

The following tutorial shows how to install AmigaOS 3.9 in an SD Card using GNU/Linux and the Amiga emulator FS-UAE.

### Step 1 - Creating empty HDF file

First thing you have to do is create an HDF file with the size of your SD card (that image will be flashed into your physical SD card in the end). So start by checking the size of you card. Open a new terminal and run fdisk like this:

    $ sudo fdisk -l

Locate your SD Card in the output of the command. It should look like this:

    Disk /dev/sdc: 29,7 GiB, 31914983424 bytes, 62333952 sectors

In my case, my card is exactly 31914983424 bytes.

Next, you must create a partitionable hard drive image file with that size (an HDF file with type RDB). You can use [hdfcreate](https://framagit.org/epifanio/hdfcreate) or [amitools' xdftool](https://amitools.readthedocs.io/en/latest/tools/xdftool.html).

If you use **hdfcreate**, you can create the HDF by running this command (replace the value of the size parameter with the size of your own card):

    $ python3 hdfcreate.py --type=rdb --size=31914983424 --filename=32gb.hdf

You should end up with a file named 32gb.hdf.

### Step 2 - Creating an AmigaOS Emergency Disk

...

### Step 3 - Creating partitions

* open FS-UAE Launcher
* my config:
  * Amiga Model: A1200, 3.1 ROM, 68020 CPU
  * Floppy Drives:
    * AmigaOS3.9-Emergency-Disk.adf -----------------> create Step 2 showing how to create Emergency Disk
  * CD-ROM Drive: AmigaOS39.iso
  * Hard-Drives:
    * choose the HDF created on Step 1
    * and also choose directory "temp" where you have downloaded and extracted pfs3 files (from aminet)
  * ROM & RAM
    * Chip RAM: 8MB
* press Start
* double click "Emergency-Disk" icon (not Emergency-Boot)
* double click "HD Toolbox"
* select "uaehf.device", click OK
* select UAEHF drive (29.7G) and click "Install Drive"
* warning "Installing a drive destroys all data on it. Do you really want to install it?", click OK
* click "Read Configuration"
* click "Install"
* click OK to ignore the warning about permanently deleting all data from drive
* click "Partition Drive"
* select each partition and click "Delete Partition" for all of them
* click "New Partition"
* change it's size to about 1GB or whatever you desire
* set partition name to DH0
* click on "Add/Update" (now we're going to change file system to PFS3)
* click "Add New File System"
* click on Drawer, type DH1: and press enter
* navigate to where you saved "pfs3aio" file, select it and click "Load"
* you should now have 2 file systems; select the one you've just added and click OK
* now you're back to the "Hard Drive Preparation and Partitioning" window - click "Change"
* on the field "File System", select "CFS\00", click OK
* back to the "Hard Drive Preparation and Partitioning", make sure you select "Bootable" option
* click on the empty space after the partition you just created and click "New Partition"
* make sure this second partition takes the rest of the disk space (unless you want to create a third partition)
* set partition name to DH1
* make sure the File System is set to "Custom File System CFS\00"
* click on "Save"
* there will be a popup warning that some partitions will be deleted, just click "Ok"
* click "Exit"
* a window pops up telling you to reboot your "amiga", click on "Reboot"

### Step 4 - Format partitions

* after the "amiga" reboots you will have  2 new icons on your desktop: DH0:Uninitialized and DH1:Uninitialized
* select "DH0:Uninitialized", press the mouse right button, go to menu Icons and select "Format disk..."
* set volume name to System (or anything else, really)
* click "Quick Format", click "Format" on the next screen and again "Format" on the Warning that pops up
* DH0: will be formated
* repeat the process for DH1:Unitialized but name it something like "Programs"
* DH1: will be formated
* reboot your "Amiga"

### Step 5 - Install AmigaOS 3.9

* Double click AmigaOS icon, open "OS-Version3.9" drawer and open "OS3.9-Installation"
* Choose option "OS3.9 full installation over OS3.0 or empty HD" and click "Proceed"
* Proceed
* Accept License Agreement
* Proceed
* Select DH0: (System:) and click Proceed
* Choose languages you want to be installed and click Proceed
* Usually you will not want to install printer drivers, so just click Proceed
* Choose the keymaps of the Amigas you want to use this installation on and click Proceed
* Choose whether you want to install backdrops on your installation; you'll have plenty of space, so you can choose Yes
* Wait while files are being copied to you OS3.9 installation
* It will take a while, so be patient, relax and enjoy the pictures like this guy surfing the Internet with OS3.9 (on a real surf board)
* Click Install to install the CD-Rom driver and the driver for your graphics system
* Click Proceed
* Remove floppy disk from drive - press F12 and eject disk from DF0: (also eject te CD-Rom)
* Click Proceed to reboot your "Amiga"

### Step 6 - Flash the HDF to the SD Card

Simply run the following instruction, replacing the value of parameter 'of' with the path of your SD card. **WARNING**: misuse of the this instruction can damage your storage, so use it carefully:

    $ dd if=32gb.hdf of=/dev/sdc bs=1024k status=progress

### Step 7

Enjoy the Amiga.

### Credits
[Amiga OS39 Installation Tutorial](https://framagit.org/epifanio/easyinstall_amigaos39) by [Tiago Epifânio](https://framagit.org/epifanio) is licensed under CC BY-SA 4.0. 
To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]()

