# Easy Install AmigaOS 3.9

Installing AmigaOS 3.9 in an SD Card using GNU/Linux and the Amiga emulator FS-UAE.

- [English](tutorial_en.md)
- [Português](tutorial_pt-pt.md)
